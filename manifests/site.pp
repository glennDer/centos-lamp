node default
{
     package{ 'httpd':
             ensure => installed,
     }
    package{ 'php': 
	     ensure => installed,
    }
     service { 'httpd':
	ensure => running,
     }	
}
